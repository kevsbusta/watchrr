'use strict';

angular.module('starter')
  .controller('loginController', function ($scope, Auth, $location, UserService) {
    $scope.facebookLogin = function() {
      console.log("here");
      $scope.err = null;
      Auth.$authWithOAuthPopup('facebook', {rememberMe: true, scope: 'email, public_profile' }).then(redirect, showError);
    };

    $scope.googleLogin = function() {
      $scope.err = null;
      Auth.$authWithOAuthPopup('google', {rememberMe: true, scope: 'email, profile' }).then(redirect, showError);
    };

    $scope.anonymousLogin = function() {
      $scope.err = null;
      Auth.$authAnonymously({rememberMe: true}).then(redirect, showError);
    };



    function redirect(authData) {
      $location.path('/app/map');
    }

    function showError(err) {
      $scope.err = err;
    }


  });
