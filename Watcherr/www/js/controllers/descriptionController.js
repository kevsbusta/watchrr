angular.module("starter")
    .controller("descriptionController", function ($scope, $state, $firebaseArray, $cordovaCamera, Auth, alertsService, UserService, descriptionService) {
        console.log('desc COntroller');
        console.log(descriptionService.id);

        FIREBASE_URL = 'https://watcherr.firebaseio.com/reports';
        var data = new Firebase(FIREBASE_URL);

        $scope.save = function () {
            var reportsRef = data.orderByChild('id').equalTo(descriptionService.id);
            reports = $firebaseArray(reportsRef);
            reports.$loaded().then(function () {
                console.log(reports.length);
                console.log(reports[0]);
                if (reports.length > 0) {
                    reports[0].description = $scope.description;
                    reports.$save(0);
                }
            });
        }

        $scope.upload = function () {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                targetWidth: 500,
                targetHeight: 500,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.uploadedImage = imageData;
                var reportsRef = data.orderByChild('id').equalTo(descriptionService.id);
                reports = $firebaseArray(reportsRef);
                reports.$loaded().then(function () {
                    console.log(reports[0]);
                    if (reports.length > 0) {
                        reports[0].image = imageData;
                        reports.$save(0);
                    }
                });
                // syncArray.$add({ image: imageData }).then(function () {
                //     alert("Image has been uploaded");
                // });
            }, function (error) {
                console.error(error);
            });
        }
        
        $scope.skip = function(){
            $state.go('app.map')
        }
    });

