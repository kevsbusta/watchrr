'use strict';

var placeSearch, autocomplete;

angular.module('starter')

  .controller('mapController', function ($scope, $firebaseArray, $timeout, reportService, NgMap, alertsService, UserService, reportFactory) {


    $scope.heatmapOn = false;
    var timeout;

    $scope.$on('reportsLoaded', function () {
      $scope.firebaseReport = reportService.firebaseReport;
      $scope.reports = reportService.reports;
      var currentDate = new Date();
      _.remove($scope.reports, function(n){ return moment(n.created_at).format("YYYY-MM-DD") != moment(currentDate).format("YYYY-MM-DD") })
      $scope.heatmap = _.map($scope.reports, function (item) {
        return new google.maps.LatLng(item.latitude, item.longitude);
      });
      $scope.firebaseReport.$watch(function () {
        alertsService.addCount(UserService.user.id);
      });

          NgMap.getMap().then(function(map) {
            var geocoder = new google.maps.Geocoder;

            google.maps.event.addListener(map, "center_changed", function() {
              reportFactory.latitude = map.getCenter().lat();
              reportFactory.longitude = map.getCenter().lng();
              $scope.latitude = map.getCenter().lat();
              $scope.longitude = map.getCenter().lng();
              var latlng = {lat: parseFloat(map.getCenter().lat()), lng: parseFloat(map.getCenter().lng())};
              geocoder.geocode({'location': latlng}, function(results, status) {
                reportFactory.address = results[0].formatted_address;
              });


            });
          }, function(res){
            console.log(res);
          });

    });



    $scope.setCategory = function (category) {
      return "img/" + category + "-icon.png";
    }

    $scope.showReportButtons = false;

    $scope.loaded = false;

    $scope.switchView = function () {
      $scope.heatmapOn = !$scope.heatmapOn;
    }

    getLocation();

    function getLocation() {

      var geocoder = new google.maps.Geocoder;

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (response) {

          $scope.currentLocation = [response.coords.latitude, response.coords.longitude];
          $scope.latitude = response.coords.latitude;
          $scope.longitude = response.coords.longitude;

          reportFactory.latitude = response.coords.latitude;
          reportFactory.longitude = response.coords.longitude;
          var latlng = {lat: parseFloat(response.coords.latitude), lng: parseFloat(response.coords.longitude)};
          geocoder.geocode({'location': latlng}, function(results, status) {
            reportFactory.address = results[0].formatted_address;
          });

          $scope.loaded = true;
          $scope.$apply();
        });

      } else {
        console.log("No location found.");
      }
    }

  });
