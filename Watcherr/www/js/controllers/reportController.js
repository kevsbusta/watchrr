DEFAULT_PROVIDER = 'facebook';

angular.module("starter")
  .controller("reportController", function ($scope, $firebaseArray, $state, reportService, reportFactory, UserService, descriptionService) {

    $scope.$on('reportsLoaded', function () {
      $scope.firebaseReport = reportService.firebaseReport;
      $scope.reports = reportService.reports;
      $scope.currentUserReports = _.filter($scope.reports, function (item) { return item.userId == UserService.user.id });
      reportService.userReportCount = $scope.currentUserReports.length;
      $scope.firebaseReport.$watch(function () {
        $scope.currentUserReports = _.filter($scope.reports, function (item) { return item.userId == UserService.user.id });

      });
    });



    $scope.reportCrime = function (name) {
      var id = UserService.user.id.toString() + reportService.userReportCount.toString();
      $scope.firebaseReport.$loaded().then(function () {
        var reportData = {
          id: id,
          userId: UserService.user.id,
          category: name,
          created_at: Date.now(),
          provider: DEFAULT_PROVIDER,
          provider_uid: 1234,
          latitude: reportFactory.latitude,
          longitude: reportFactory.longitude,
          address: reportFactory.address,
          description: ''
        }

        $scope.firebaseReport.$add(reportData).then(function () {
          descriptionService.id = id;
          $state.go('app.description');
        });
      });
    };

  });
