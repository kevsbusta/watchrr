angular.module("starter")
    .controller("appController", function ($scope, $state, Auth, alertsService, UserService) {
        $scope.logout = function () { Auth.$unauth(); };

        $scope.$on('Userloaded', function () {
            alertsService.getAlerts(UserService.user.id);
        });

        $scope.$on('alertsChanged', function () {
            console.log('alertsCHanged')
            $scope.alerts = alertsService.alertsCount;
        });
        
        $scope.alertClick = function(){
            alertsService.resetCount(UserService.user.id);
            $scope.alerts = 0;
            $state.go('app.alerts');
            
        }
    });

