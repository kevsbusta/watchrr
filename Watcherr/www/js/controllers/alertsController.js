angular.module("starter")
    .controller("alertsController", function ($rootScope, $scope, Auth, alertsService, UserService, reportService) {
        if (UserService.user) {
            alertsService.resetCount(UserService.user.id);
        }

        $scope.firebaseReport = reportService.firebaseReport;
        $scope.reports = reportService.reports;
        $scope.reportsToday = _.filter($scope.reports, function (item) { return item.id >= 1461440924000 })


        $scope.$on('alertsChanged', function () {
            console.log('here');
            $scope.reportsToday = _.filter($scope.reports, function (item) { return item.id >= 1461440924000 })
            console.log($scope.reportsToday);
        });

    });
