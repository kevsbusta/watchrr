(function () {
    'use strict';
    angular.module('starter')

        .factory('reportService', function ($rootScope, $firebaseArray, alertsService) {

            var reportsUrl = 'https://watcherr.firebaseio.com/users';

            //var reportsRef = new Firebase(reportsUrl);

            var Reports = {
                firebaseReport: undefined,
                reports: undefined,
                reportsToday: undefined,
                totalReportCount: undefined,
                userReportCount: undefined
            }

            FIREBASE_URL = 'https://watcherr.firebaseio.com/reports';
            var data = new Firebase(FIREBASE_URL);

            Reports.firebaseReport = $firebaseArray(data);
            Reports.firebaseReport.$loaded().then(function () {
                Reports.reports = Reports.firebaseReport;
                $rootScope.$broadcast('reportsLoaded')
            });
            
            // getReportsToday();
            
            // function getReportsToday(){
            //     var now = Date.now();
            //     var ref = data.orderByChild('created_at').startAt(now);
            //     var reportNow = $firebaseArray(ref);
            //     reportNow.$loaded().then(function(){
            //        console.log(reportNow); 
            //        Reports.reportsToday = reportNow;
            //        $rootScope.$broadcast('reportsTodayLoaded')
            //     });
            // }

            return Reports;
        });
})();
