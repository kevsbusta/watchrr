(function () {
    'use strict';
    angular.module('starter')

        .factory('alertsService', function ($rootScope, $firebaseArray) {

            var Alerts = {
                firebaseAlerts: undefined,
                alertsCount: undefined,
                saveFirstRecord: saveFirstRecord,
                resetCount: resetCount,
                addCount: addCount,
                getAlerts: getAlerts
            }
            
            FIREBASE_URL = 'https://watcherr.firebaseio.com/alerts';
            var data = new Firebase(FIREBASE_URL);
            
            
            function getAlerts(userId) {
                var reportsRef = data.orderByChild('id').equalTo(userId);
                var alerts = $firebaseArray(reportsRef);

                alerts.$loaded().then(function () {
                    Alerts.alertsCount = alerts[0].alertsCount;
                    $rootScope.$broadcast('alertsChanged');
                });
            }

            

            function saveFirstRecord(userId) {
                var alerts = $firebaseArray(data);
                alerts.$loaded().then(function () {
                    alerts.$add({ id: userId, alertsCount: 0 });
                });
            }

            function resetCount(userId) {
                var reportsRef = data.orderByChild('id').equalTo(userId);
                var alerts = $firebaseArray(reportsRef);

                alerts.$loaded().then(function () {
                    if (alerts.length != 0) {
                        alerts[0].alertsCount = 0;
                        alerts.$save(0);
                        $rootScope.$broadcast('alertsChanged');
                    }
                });
            }

            function addCount(userId) {
                var reportsRef = data.orderByChild('id').equalTo(userId);
                var alerts = $firebaseArray(reportsRef);

                alerts.$loaded().then(function () {
                    if (alerts.length != 0) {
                        alerts[0].alertsCount++;
                        Alerts.alertsCount = alerts[0].alertsCount;

                        alerts.$save(0);
                        $rootScope.$broadcast('alertsChanged');
                    }
                });
            }

            return Alerts;
        });
})();
