angular.module("starter")
.factory("Items", function($firebaseArray) {
  var itemsRef = new Firebase('https://watcherr.firebaseio.com/');
  return $firebaseArray(itemsRef);
})