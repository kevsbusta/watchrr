(function() {
  'use strict';
  angular.module('starter')

    .factory('UserService', function($rootScope, $q, $firebaseObject, Ref, $firebaseArray, alertsService) {
        
        var reportsUrl = 'https://watcherr.firebaseio.com/users';
        
        //var reportsRef = new Firebase(reportsUrl);
        
        var User = {
            setUser: setUser,
            user: undefined,
            profile: undefined
        }
        
        var $scope = {};
        var users;
        
        function setUser(user){
            User.profile = $firebaseObject(Ref.child('users/'+user.uid));
            User.user = user[user.provider];
            var reportsRef = new Firebase(reportsUrl).orderByChild('id').equalTo(User.user.id);
            users = $firebaseArray(reportsRef);
            users.$loaded().then(function(){
                if(users.length == 0){
                    users.$add({id: User.user.id});
                    alertsService.saveFirstRecord(User.user.id);
                }
            });
            
            $rootScope.$broadcast('Userloaded');
        }
     
        return User;
    });
})();
