// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

FIREBASE_URL = 'https://watcherr.firebaseio.com/';
DEFAULT_PROVIDER = 'facebook';

angular.module('starter', [
    'ionic',
    'firebase',
    'ui.router',
    'firebase.auth',
    'firebase.config',
    'firebase.ref',
    'ngMap',
    'ngCordova'])
    .run(function ($ionicPlatform, $rootScope, $location, Auth, SECURED_ROUTES, loginRedirectPath, UserService) {


        // watch for login status changes and redirect if appropriate
        Auth.$onAuth(check);

        // some of our routes may reject resolve promises with the special {authRequired: true} error
        // this redirects to the login page whenever that is encountered
        $rootScope.$on('$routeChangeError', function (e, next, prev, err) {
            if (err === 'AUTH_REQUIRED') {
                $location.path(loginRedirectPath);
            }
        });

        function check(user) {
            if (!user) {
                $location.path('/login');
            } else {
                UserService.setUser(user);
                $location.path('/app/map');
            }
        }

        function authRequired(path) {
            return SECURED_ROUTES.hasOwnProperty(path);
        }

        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/app.html',
                controller: 'appController'
            })

            .state('app.map', {
                url: '/map',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/map.html',
                        controller: 'mapController'
                    }
                }

            })

            .state('app.alerts', {
                url: '/alerts',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/alerts.html',
                        controller: 'alertsController',
                    }
                }

            })
            .state('app.history', {
                url: '/history',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/history.html',
                        controller: 'historyController',
                    }
                }

            })

             .state('app.description', {
                url: '/desc',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/description.html',
                        controller: 'descriptionController',
                    }
                }

            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'loginController'
            });

        $urlRouterProvider.otherwise('/login');
    }])
    .constant('SECURED_ROUTES', {});
